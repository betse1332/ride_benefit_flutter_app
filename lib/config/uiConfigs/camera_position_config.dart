import 'package:google_maps_flutter/google_maps_flutter.dart';

class CameraPositionConfig {
  static CameraPosition getCameraPosition(LatLng target) {
    return CameraPosition(target: target, zoom: 11.0, tilt: 0, bearing: 0);
  }
}
