import 'package:flutter/widgets.dart';

class SizeConfig {
  static dynamic _mediaQueryData;
  static double screenWidth = 0.0;
  static double screenHeight = 0.0;

  // static double defaultSize;
  // static Orientation orientation;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenHeight = _mediaQueryData.size.height;
    screenWidth = _mediaQueryData.size.width;
  }

  double getProportionateHeight(double inputHeight) {
    double screenHeight = SizeConfig.screenHeight;
    return (inputHeight / 812.0) * screenHeight;
  }

  double getProportionateWidth(double inputWidth) {
    double screenWidth = SizeConfig.screenWidth;

    return (inputWidth / 375.0) * screenWidth;
  }
}
