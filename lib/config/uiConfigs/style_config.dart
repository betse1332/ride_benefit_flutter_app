import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

EdgeInsets iconPadding = EdgeInsets.symmetric(
    horizontal: SizeConfig().getProportionateWidth(3),
    vertical: SizeConfig().getProportionateHeight(3));

class AppStyleConfig {
  static ButtonStyle getElevatedButtonStyle(
      Color btnColor, double roundedRadius,
      [double btnPadding = 0]) {
    return ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
            EdgeInsets.symmetric(
                vertical: SizeConfig().getProportionateHeight(btnPadding))),
        backgroundColor: MaterialStateProperty.all<Color>(btnColor),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                    SizeConfig().getProportionateHeight(roundedRadius)))));
  }
}
