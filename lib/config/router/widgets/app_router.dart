import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';

class AppRouter extends RouterDelegate with PopNavigatorRouterDelegateMixin {
  @override
  final GlobalKey<NavigatorState> navigatorKey;
  final PageManager pageManager;

  AppRouter({required this.pageManager})
      : navigatorKey = GlobalKey<NavigatorState>();

  @override
  void addListener(VoidCallback listener) {
    // TODO: implement addListener
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RouterBloc, RouterState>(listener: (context, state) {
      print("State changed ************ ${state.toString()}");
    }, builder: (context, state) {
      return Navigator(
          key: navigatorKey, onPopPage: handleOnPop, pages: pages(state));
    });
  }

  bool handleOnPop(Route<dynamic> route, result) {
    final routerBloc =
        BlocProvider.of<RouterBloc>(navigatorKey.currentContext!);
    final state = routerBloc.state;

    if (route.settings.name == service) {
      routerBloc.add(NavigateTo(page: dashBoardPageConfig));
    }
    if (route.settings.name == serviceCenter) {
      routerBloc.add(NavigateToService(
          pageConfig: servicePageConfig,
          serviceNavConfig: (state as ServiceRouteState).serviceNavConfig));
    }
    if (route.settings.name == packageDetail) {
      routerBloc.add(NavigateToServiceCenter(
        pageConfig: serviceCenterPageConfig,
        serviceNavConfig: (state as ServiceRouteState).serviceNavConfig,
      ));
    }
    if (route.settings.name == packageBooking) {
      routerBloc.add(NavigateToPackageDetail(
          pageConfig: packageDetailPageConfig,
          serviceNavConfig: (state as ServiceRouteState).serviceNavConfig));
    }
    if (route.settings.name == notifications) {
      final state = (routerBloc.state) as NotificationRoute;

      routerBloc.add(NavigateToNotificationPopRoute(
          pageConfig: state.notificationPopConfig.pageConfig,
          serviceNavConfig: state.notificationPopConfig.serviceConfig));
    }
    if (route.settings.name == account ||
        route.settings.name == about ||
        route.settings.name == history) {
      routerBloc.add(NavigateTo(page: profilePageConfig));
    }
    if (!route.didPop(result)) {
      return false;
    }

    return true;
  }

  List<Page> pages(RouterState state) {
    return [
      if (state is RouteInitial) pageManager.addPage(splashPageConfig),
      if (state is SimpleRouting) pageManager.addPage(state.pageConfig),
      if (state is ServiceRoute)
        pageManager.addPage(state.pageConfig, state.serviceNavConfig),
      if (state is ServiceCenterRoute)
        pageManager.addPage(
          state.pageConfig,
          state.serviceNavConfig,
        ),
      if (state is PackageDetailRoute)
        pageManager.addPage(state.pageConfig, state.serviceNavConfig),
      if (state is PackageBookingRoute)
        pageManager.addPage(state.pageConfig, state.serviceNavConfig),
      if (state is NotificationRoute) pageManager.addPage(state.pageConfig),
      if (state is NotificationPopRoute)
        pageManager.addPage(state.pageConfig, state.serviceNavConfig)
    ];
  }

  static void bottomNavigationHelper(
    int tabIndex,
    BuildContext context,
  ) {
    final routerBloc = BlocProvider.of<RouterBloc>(context);
    switch (tabIndex) {
      case 0:
        routerBloc.add(NavigateTo(page: dashBoardPageConfig));
        break;
      case 1:
        routerBloc.add(NavigateTo(page: bookingsPageConfig));
        break;
      case 2:
        routerBloc.add(NavigateTo(page: profilePageConfig));
        break;
    }
  }

  @override
  void removeListener(VoidCallback listener) {
    // TODO: implement removeListener
  }

  @override
  Future<void> setNewRoutePath(configuration) {
    // TODO: implement setNewRoutePath
    throw UnimplementedError();
  }
}
