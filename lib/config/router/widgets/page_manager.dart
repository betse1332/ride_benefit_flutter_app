import 'package:flutter/material.dart';
import 'package:ride_benefit/config/router/router.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/modules/modules.dart';

class PageManager {
  MaterialPage _createPage(Widget child, PageConfiguration pageConfig) {
    return MaterialPage(
      name: pageConfig.path,
      key: pageConfig.key,
      child: child,
    );
  }

  MaterialPage addPage(PageConfiguration pageConfig,
      [ServiceNavConfiguration? serviceNavConfig]) {
    switch (pageConfig.page) {
      case Pages.Splash:
        return _createPage(Splash(), splashPageConfig);

      case Pages.Login:
        return _createPage(LoginScreen(), loginPageConfig);

      case Pages.DashBoard:
        return _createPage(HomeScreen(), dashBoardPageConfig);
      case Pages.Service:
        return _createPage(
            ServiceScreen(
                serviceCategory:
                    serviceCategories[serviceNavConfig!.selectedService]),
            servicePageConfig);
      case Pages.ServiceCenter:
        return _createPage(
            ServiceCenterScreen(
                serviceCenter:
                    serviceCenters[serviceNavConfig!.selectedServiceCenter]),
            serviceCenterPageConfig);
      case Pages.PackageDetail:
        return _createPage(
            PackageScreen(
                serviceCenter:
                    serviceCenters[serviceNavConfig!.selectedServiceCenter]),
            packageDetailPageConfig);
      case Pages.PackageBooking:
        return _createPage(
            PackageBookingScreen(
              serviceCenter:
                  serviceCenters[serviceNavConfig!.selectedServiceCenter],
            ),
            packageBookingPageConfig);
      case Pages.Bookings:
        return _createPage(
            BookingsScreen(
              serviceCenter: serviceCenters[0],
            ),
            bookingsPageConfig);
      case Pages.Profile:
        return _createPage(
            ProfileScreen(
              profileModel: userProfile,
            ),
            profilePageConfig);
      case Pages.BookingStatus:
        return _createPage(BookingStatusScreen(), bookingStatusPageConfig);
      case Pages.About:
        return _createPage(
            AccountScreen(
              profileModel: userProfile,
            ),
            accountPageConfig);
      case Pages.History:
        return _createPage(
            HistoryScreen(
              historyModel: histories,
            ),
            historyPageConfig);
      case Pages.Notifications:
        return _createPage(
            NotificationScreen(
              notifications: notificationsMock,
            ),
            notificationPageConfig);
      case Pages.Settings:
        return _createPage(
            AccountScreen(
              profileModel: userProfile,
            ),
            accountPageConfig);
      case Pages.Account:
        return _createPage(
            AccountScreen(
              profileModel: userProfile,
            ),
            accountPageConfig);
    }
  }
}
