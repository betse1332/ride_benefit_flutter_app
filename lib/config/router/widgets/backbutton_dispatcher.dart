import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';

class AppBackButtonDispatcher extends RootBackButtonDispatcher {
  final AppRouter routerDelegate;

  AppBackButtonDispatcher({required this.routerDelegate}) : super();

  @override
  Future<bool> didPopRoute() async {
    final navContext = routerDelegate.navigatorKey.currentContext!;
    final routerState = BlocProvider.of<RouterBloc>(navContext).state;

    Route currentRoute =
        routerDelegate.pages(routerState)[0].createRoute(navContext);

    final routeName = currentRoute.settings.name;

    return (routeName != dashBoard &&
            routeName != bookings &&
            routeName != login &&
            routeName != bookingStatus &&
            routeName != profile)
        ? routerDelegate.handleOnPop(currentRoute, true)
        : Navigator.canPop(navContext);
  }
}
