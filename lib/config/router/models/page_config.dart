import 'package:flutter/cupertino.dart';

import 'models.dart';

class PageConfiguration {
  final ValueKey key;
  final String path;
  final Pages page;
  final int selectedTab;

  PageConfiguration(
      {required this.key,
      required this.path,
      required this.page,
      this.selectedTab = BottomNavTab.dashboard});
}
