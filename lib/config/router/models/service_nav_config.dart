class ServiceNavConfiguration {
  final int selectedService;
  final int selectedServiceCenter;
  final int selectedPackage;

  ServiceNavConfiguration(
      {this.selectedService = 0,
      this.selectedServiceCenter = 0,
      this.selectedPackage = 0});

  ServiceNavConfiguration copyWith(
      {int? selectedService,
      int? selectedServiceCenter,
      int? selectedPackage}) {
    return ServiceNavConfiguration(
        selectedService: selectedService ?? this.selectedService,
        selectedServiceCenter:
            selectedServiceCenter ?? this.selectedServiceCenter,
        selectedPackage: selectedPackage ?? this.selectedPackage);
  }
}
