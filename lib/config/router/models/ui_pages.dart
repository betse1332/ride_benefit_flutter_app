import 'package:flutter/cupertino.dart';
import 'package:ride_benefit/config/config.dart';

enum Pages {
  Splash,
  Login,
  DashBoard,
  Service,
  ServiceCenter,
  PackageDetail,
  PackageBooking,
  Bookings,
  Profile,
  BookingStatus,
  Account,
  Notifications,
  History,
  Settings,
  About
}

PageConfiguration splashPageConfig =
    PageConfiguration(key: ValueKey(splash), path: splash, page: Pages.Splash);
PageConfiguration loginPageConfig =
    PageConfiguration(key: ValueKey(login), path: login, page: Pages.Login);
PageConfiguration dashBoardPageConfig = PageConfiguration(
    key: ValueKey(dashBoard),
    path: dashBoard,
    page: Pages.DashBoard,
    selectedTab: BottomNavTab.dashboard);

PageConfiguration servicePageConfig = PageConfiguration(
    key: ValueKey(service),
    path: service,
    page: Pages.Service,
    selectedTab: BottomNavTab.dashboard);
PageConfiguration serviceCenterPageConfig = PageConfiguration(
    key: ValueKey(serviceCenter),
    path: serviceCenter,
    page: Pages.ServiceCenter,
    selectedTab: BottomNavTab.dashboard);

PageConfiguration packageDetailPageConfig = PageConfiguration(
    key: ValueKey(packageDetail),
    path: packageDetail,
    page: Pages.PackageDetail,
    selectedTab: BottomNavTab.dashboard);

PageConfiguration packageBookingPageConfig = PageConfiguration(
    key: ValueKey(packageBooking),
    path: packageBooking,
    page: Pages.PackageBooking,
    selectedTab: BottomNavTab.dashboard);

PageConfiguration bookingsPageConfig = PageConfiguration(
    key: ValueKey(bookings),
    path: bookings,
    page: Pages.Bookings,
    selectedTab: BottomNavTab.bookings);
PageConfiguration profilePageConfig = PageConfiguration(
    key: ValueKey(profile),
    path: profile,
    page: Pages.Profile,
    selectedTab: BottomNavTab.profile);
PageConfiguration bookingStatusPageConfig = PageConfiguration(
    key: ValueKey(bookingStatus),
    path: bookingStatus,
    page: Pages.BookingStatus,
    selectedTab: BottomNavTab.bookings);
PageConfiguration accountPageConfig = PageConfiguration(
    key: ValueKey(account),
    path: account,
    page: Pages.Account,
    selectedTab: BottomNavTab.profile);
PageConfiguration historyPageConfig = PageConfiguration(
    key: ValueKey(history),
    path: history,
    page: Pages.History,
    selectedTab: BottomNavTab.profile);
PageConfiguration settingsPageConfig = PageConfiguration(
    key: ValueKey(settings),
    path: settings,
    page: Pages.Settings,
    selectedTab: BottomNavTab.profile);
PageConfiguration aboutPageConfig = PageConfiguration(
    key: ValueKey(about),
    path: about,
    page: Pages.About,
    selectedTab: BottomNavTab.profile);
PageConfiguration notificationPageConfig = PageConfiguration(
    key: ValueKey(notifications),
    path: notifications,
    page: Pages.Notifications,
    selectedTab: BottomNavTab.profile);
// ServiceNavConfiguration healthCareNavConfig=ServiceNavConfiguration()
