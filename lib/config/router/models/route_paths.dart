const String splash = '/splash';
const String login = "/login";
const String dashBoard = "/dashboard";
const String service = "/service";
const String serviceCenter = "/serviceCenter";
const String packageDetail = "/packageDetail";
const String packageBooking = "/packageBooking";
const String bookings = "/bookings";
const String profile = "/profile";
const String bookingStatus = "/bookingStatus";

const String account = "/account";
const String history = "/history";
const String notifications="/notifications";
const String settings = "/settings";
const String about = "/about";
