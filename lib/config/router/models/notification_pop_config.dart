import 'package:ride_benefit/config/config.dart';

class NotificationPopConfig {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceConfig;

  NotificationPopConfig({required this.pageConfig,required this.serviceConfig});
}
