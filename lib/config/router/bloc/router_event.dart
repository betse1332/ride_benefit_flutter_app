import 'package:equatable/equatable.dart';

import '../router.dart';

abstract class RouterEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class NavigateTo extends RouterEvent {
  final PageConfiguration page;

  NavigateTo({required this.page});

  @override
  String toString() {
    return "Navigate To:${page.path}";
  }
}

class NavigateToService extends RouterEvent {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  NavigateToService({required this.pageConfig, required this.serviceNavConfig});

  @override
  String toString() {
    return "Navigate To:${pageConfig.path}";
  }
}

class NavigateToServiceCenter extends RouterEvent {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  NavigateToServiceCenter(
      {required this.pageConfig, required this.serviceNavConfig});
}

class NavigateToPackageDetail extends RouterEvent {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  NavigateToPackageDetail(
      {required this.pageConfig, required this.serviceNavConfig});
}

class NavigateToPackageBooking extends RouterEvent {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  NavigateToPackageBooking(
      {required this.pageConfig, required this.serviceNavConfig});
}

class NavigateToNotification extends RouterEvent {
  final PageConfiguration pageConfig;
  final NotificationPopConfig notificationPopConfig;

  NavigateToNotification(
      {required this.pageConfig, required this.notificationPopConfig});
}

class NavigateToNotificationPopRoute extends RouterEvent {
  final PageConfiguration pageConfig;

  final ServiceNavConfiguration serviceNavConfig;

  NavigateToNotificationPopRoute(
      {required this.pageConfig,required this.serviceNavConfig});
}
