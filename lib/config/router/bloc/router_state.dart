import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:ride_benefit/config/config.dart';

abstract class RouterState extends Equatable {
  @override
  List<Object?> get props => [];
}

class ServiceRouteState extends RouterState {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  ServiceRouteState({required this.pageConfig, required this.serviceNavConfig});

  @override
  List<Object?> get props => [this.pageConfig, this.serviceNavConfig];

  @override
  String toString() {
    return "Routing Success: ${pageConfig.path} ,Selected service :${serviceNavConfig.selectedService}";
  }
}

class RouteLoading extends RouterState {}

class RouteInitial extends RouterState {}

class RoutingSuccess extends RouterState {
  final PageConfiguration currentRoute;

  RoutingSuccess({required this.currentRoute});

  @override
  String toString() {
    return "Routing Success: ${currentRoute.path}";
  }
}

class SimpleRouting extends RouterState {
  final PageConfiguration pageConfig;

  SimpleRouting({required this.pageConfig});

  @override
  List<Object?> get props => [this.pageConfig];

  @override
  String toString() {
    return "Routing Success: ${pageConfig.path}";
  }
}

class ServiceRoute extends ServiceRouteState {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  ServiceRoute({required this.pageConfig, required this.serviceNavConfig})
      : super(pageConfig: pageConfig, serviceNavConfig: serviceNavConfig);
}

class ServiceCenterRoute extends ServiceRouteState {
  final PageConfiguration pageConfig;

  final ServiceNavConfiguration serviceNavConfig;

  ServiceCenterRoute({
    required this.pageConfig,
    required this.serviceNavConfig,
  }) : super(pageConfig: pageConfig, serviceNavConfig: serviceNavConfig);
}

class PackageDetailRoute extends ServiceRouteState {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  PackageDetailRoute({required this.pageConfig, required this.serviceNavConfig})
      : super(pageConfig: pageConfig, serviceNavConfig: serviceNavConfig);
}

class PackageBookingRoute extends ServiceRouteState {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  PackageBookingRoute(
      {required this.pageConfig, required this.serviceNavConfig})
      : super(pageConfig: pageConfig, serviceNavConfig: serviceNavConfig);
}

class NotificationPopRoute extends ServiceRouteState {
  final PageConfiguration pageConfig;
  final ServiceNavConfiguration serviceNavConfig;

  NotificationPopRoute(
      {required this.pageConfig, required this.serviceNavConfig})
      : super(pageConfig: pageConfig, serviceNavConfig: serviceNavConfig);
}

class NotificationRoute extends RouterState {
  final PageConfiguration pageConfig;
  final NotificationPopConfig notificationPopConfig;

  NotificationRoute(
      {required this.pageConfig, required this.notificationPopConfig});

  @override
  List<Object?> get props => [this.pageConfig, this.notificationPopConfig];
}

class RoutingFailure extends RouterState {}
