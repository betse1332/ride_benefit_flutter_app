import 'package:flutter_bloc/flutter_bloc.dart';

import '../router.dart';

class RouterBloc extends Bloc<RouterEvent, RouterState> {
  RouterBloc() : super(RouteInitial());

  @override
  Stream<RouterState> mapEventToState(event) async* {
    if (event is NavigateTo) {
      yield* _mapNavigateToEventToState(event);
    }
    if (event is NavigateToService) {
      yield* _mapNavigateToServiceEventToState(event);
    }
    if (event is NavigateToServiceCenter) {
      yield* _mapNavigateToServiceCenterEventToState(event);
    }
    if (event is NavigateToPackageDetail) {
      yield* _mapNavigateToPackageEventDetail(event);
    }
    if (event is NavigateToPackageBooking) {
      yield* _mapNavigateToPackageEventBooking(event);
    }
    if (event is NavigateToNotificationPopRoute) {
      yield* _mapNavigateToNotificationPopEventToState(event);
    }
    if (event is NavigateToNotification) {
      yield* _mapNavigateToNotificationEventToState(event);
    }
  }

  Stream<RouterState> _mapNavigateToEventToState(NavigateTo event) async* {
    yield SimpleRouting(pageConfig: event.page);
  }

  Stream<RouterState> _mapNavigateToServiceEventToState(
      NavigateToService event) async* {
    yield ServiceRoute(
        pageConfig: event.pageConfig, serviceNavConfig: event.serviceNavConfig);
  }

  Stream<RouterState> _mapNavigateToServiceCenterEventToState(
      NavigateToServiceCenter event) async* {
    yield ServiceCenterRoute(
        pageConfig: event.pageConfig, serviceNavConfig: event.serviceNavConfig);
  }

  Stream<RouterState> _mapNavigateToPackageEventDetail(
      NavigateToPackageDetail event) async* {
    yield PackageDetailRoute(
        pageConfig: event.pageConfig, serviceNavConfig: event.serviceNavConfig);
  }

  Stream<RouterState> _mapNavigateToPackageEventBooking(
      NavigateToPackageBooking event) async* {
    yield PackageBookingRoute(
        pageConfig: event.pageConfig, serviceNavConfig: event.serviceNavConfig);
  }

  Stream<RouterState> _mapNavigateToNotificationEventToState(
      NavigateToNotification event) async* {
    yield NotificationRoute(
        pageConfig: event.pageConfig,
        notificationPopConfig: event.notificationPopConfig);
  }

  Stream<RouterState> _mapNavigateToNotificationPopEventToState(
      NavigateToNotificationPopRoute event) async* {
    yield NotificationPopRoute(
        pageConfig: event.pageConfig, serviceNavConfig: event.serviceNavConfig);
  }
}
