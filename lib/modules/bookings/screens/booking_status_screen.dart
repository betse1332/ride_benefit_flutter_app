import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/modules/bookings/bookings.dart';

class BookingStatusScreen extends StatelessWidget {
  const BookingStatusScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: BookingStatusBody()),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
