import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/modules/modules.dart';

class BookingsScreen extends StatelessWidget {
  final ServiceCenter serviceCenter;

  const BookingsScreen({required this.serviceCenter, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(bookings_title),
        actions: [NotificationCounter()],
      ),
      body: BookingsBody(
        serviceCenter: serviceCenter,
      ),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
