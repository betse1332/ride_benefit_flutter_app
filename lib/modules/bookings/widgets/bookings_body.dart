import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/modules/modules.dart';

class BookingsBody extends StatelessWidget {
  final ServiceCenter serviceCenter;

  const BookingsBody({required this.serviceCenter, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          BookingsItem(serviceCenter: serviceCenter),
          BookingsItem(serviceCenter: serviceCenter),
          BookingsItem(serviceCenter: serviceCenter),



        ],
      )
    );
  }
}
