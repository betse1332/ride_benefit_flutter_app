import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

class BookingStatusBody extends StatelessWidget {
  const BookingStatusBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.screenHeight * .38,
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().getProportionateWidth(30)),
      child: Card(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig().getProportionateWidth(20)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: SizeConfig().getProportionateHeight(25)),
                child: Text(
                  congratulations_text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig().getProportionateHeight(25)),
                ),
              ),
              Text(
                success_msg,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Palette.kSecondaryColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              Divider(
                thickness: SizeConfig().getProportionateHeight(1.5),
                height: SizeConfig().getProportionateHeight(45),
              ),
              Padding(
                padding: EdgeInsets.only(
                    bottom: SizeConfig().getProportionateHeight(40)),
                child: Text(
                  check_msg,
                  style: TextStyle(color: Palette.kSecondaryColor),
                ),
              ),
              ElevatedButton(
                  style: AppStyleConfig.getElevatedButtonStyle(
                      kPrimaryColor, roundedRadius_10, 17),
                  onPressed: () {
                    BlocProvider.of<RouterBloc>(context)
                        .add(NavigateTo(page: dashBoardPageConfig));
                  },
                  child: Text(
                    go_to_home_btn,
                    style: TextStyle(
                        color: Palette.kSecondaryColor.shade50,
                        fontWeight: FontWeight.bold),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
