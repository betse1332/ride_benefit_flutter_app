import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/modules/packageDetail/widgets/working_hour_widget.dart';

class BookingsItem extends StatelessWidget {
  final ServiceCenter serviceCenter;

  const BookingsItem({required this.serviceCenter, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: SizeConfig().getProportionateHeight(15),
          horizontal: SizeConfig().getProportionateWidth(15)),
      child: Container(
        height: SizeConfig.screenHeight * .37,
        child: Card(
          elevation: 5,
          child: Column(
            children: [
              PackageDetail(serviceCenter: serviceCenter),
              PackageFragment(
                serviceCenter: serviceCenter,
                child: SpanWidget(
                  spanTime: DateTime.now().day,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
