class HistoryModel {
  final String serviceCenterName;
  final String category;
  final int discount;
  final String address;
  final DateTime date;

  HistoryModel(
      {required this.serviceCenterName,
      required this.category,
      required this.discount,
      required this.address,
      required this.date});
}
