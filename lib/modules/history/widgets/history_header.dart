import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/utils/palette.dart';

class HistoryHeader extends StatelessWidget {
  const HistoryHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        TextButton(
            style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.symmetric(
                    horizontal: SizeConfig().getProportionateWidth(10)))),
            onPressed: () {},
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: [
                WidgetSpan(
                    
                    child: Icon(
                      Icons.filter_list,
                      size: SizeConfig().getProportionateHeight(25),
                    )),
                WidgetSpan(
                  child: Text(
                    " Filter",
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                        fontSize:SizeConfig().getProportionateWidth(18) ),
                  ),
                )
              ]),
            )),
        TextButton(
            style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.symmetric(
                    horizontal: SizeConfig().getProportionateWidth(20)))),
            onPressed: () {},
            child: RichText(
              text: TextSpan(children: [
                WidgetSpan(
                    child: Icon(
                  Icons.sort,
                  size: SizeConfig().getProportionateHeight(25),
                )),
                WidgetSpan(
                  child: Text(
                    " Sort",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize:SizeConfig().getProportionateWidth(18) ),
                  ),
                )
              ]),
            ))
      ],
    );
  }
}
