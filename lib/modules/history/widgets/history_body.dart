import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';

class HistoryBody extends StatelessWidget {
  final List<HistoryModel> historyModel;

  const HistoryBody({required this.historyModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().getProportionateWidth(12)),
      child: Column(
        children: [
          HistoryHeader(),
          Padding(
            padding: EdgeInsets.only(
                left: SizeConfig().getProportionateWidth(5),
                right: SizeConfig().getProportionateWidth(5),
                bottom: SizeConfig().getProportionateHeight(10)),
            child: Divider(
              height: 0,
              thickness: 1.5,
            ),
          ),
          Expanded(
            child: ListView.builder(
                itemCount: historyModel.length,
                itemBuilder: (context, index) {
                  return HistoryCard(historyModel: historyModel[index]);
                }),
          ),
        ],
      ),
    );
  }
}
