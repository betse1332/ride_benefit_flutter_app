import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/utils/datetime_util.dart';
import 'package:ride_benefit/utils/palette.dart';

class HistoryCard extends StatelessWidget {
  final HistoryModel historyModel;

  const HistoryCard({required this.historyModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: 0, vertical: SizeConfig().getProportionateHeight(5)),
      child: Card(
        // margin: EdgeInsets.zero,

        elevation: 3,
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig().getProportionateWidth(15)),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        historyModel.serviceCenterName,
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                      subtitle: Text(
                        historyModel.category,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Palette.kSecondaryColor.shade600),
                      ),
                    ),
                  ),
                  Text(
                    "${historyModel.discount}% Off",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: SizeConfig().getProportionateHeight(18),
                        color: Palette.kSecondaryColor.shade800),
                  )
                ],
              ),
              Divider(
                height: 0,
                thickness: 1.5,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: SizeConfig().getProportionateHeight(15)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      historyModel.address,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    Text(
                      DateTimeUtil.formatDatetime(historyModel.date),
                      style: TextStyle(fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
