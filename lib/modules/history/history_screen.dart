import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';

class HistoryScreen extends StatelessWidget {
  final List<HistoryModel> historyModel;

  const HistoryScreen({required this.historyModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          titleSpacing: 0,
          leading: IconButton(
              icon: Icon(
                Icons.chevron_left,
                size: SizeConfig().getProportionateHeight(35),
              ),
              onPressed: () {
                Navigator.pop(context, true);
              }),
          title: Text("History")),
      body: HistoryBody(
        historyModel: historyModel,
      ),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
