import 'package:flutter/material.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/utils/utils.dart';

class ProfileDataMapper {
  static Map<String, IconData> mapProfileData(ProfileModel profileModel) {
    return <String, IconData>{
      "${profileModel.firstName} ${profileModel.lastName}": CustomIcons.user,
      "+251 ${profileModel.phoneNum}": Icons.phone_outlined,
      profileModel.email: Icons.mail_outlined,
      "***********": Icons.lock_outlined
    };
  }
}
