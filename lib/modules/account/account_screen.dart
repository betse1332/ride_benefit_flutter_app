import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';


class AccountScreen extends StatelessWidget {
  final ProfileModel profileModel;
  const AccountScreen({required this.profileModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
            titleSpacing: 0,
            leading: IconButton(
                icon: Icon(
                  Icons.chevron_left,
                  size: SizeConfig().getProportionateHeight(35),
                ),
                onPressed: () {
                  Navigator.pop(context, true);
                }),
            title: Text("Account Information")),
        body: AccountBody(profileModel: profileModel,),
        bottomNavigationBar: BottomNavBar(),

    );
  }
}
