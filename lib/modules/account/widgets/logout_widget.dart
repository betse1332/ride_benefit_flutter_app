import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/utils/palette.dart';

class LogoutWidget extends StatelessWidget {
  const LogoutWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.screenWidth*.4,

      padding: EdgeInsets.symmetric(
          vertical: SizeConfig().getProportionateHeight(40),
          horizontal: SizeConfig().getProportionateWidth(20)),
      child: TextButton(

        onPressed: () {},
        child: Row(
          children: [
            Icon(
              Icons.logout,
              color: Colors.red.shade700,
            ),
            Text(
              "  Logout",
              style: TextStyle(
                  color: Colors.red.shade700,
                  fontSize: SizeConfig().getProportionateHeight(17),
                  fontWeight: FontWeight.w600),
            )
          ],
        ),
      ),
    );
  }
}
