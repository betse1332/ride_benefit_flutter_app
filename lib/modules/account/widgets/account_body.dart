import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';

class AccountBody extends StatelessWidget {
  final ProfileModel profileModel;

  const AccountBody({required this.profileModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ProfileHeader(
          profileModel: profileModel,
          widgetSize: .29,
        ),
        Expanded(
            child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: SizeConfig().getProportionateHeight(10),
              horizontal: SizeConfig().getProportionateWidth(20)),
          child: AccountBottom(
            profileModel: profileModel,
          ),
        )),
        LogoutWidget()
      ],
    );
  }
}
