import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/utils/palette.dart';

class AccountBottom extends StatelessWidget {
  final ProfileModel profileModel;

  const AccountBottom({required this.profileModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mappedData = ProfileDataMapper.mapProfileData(profileModel);
    return ListView.separated(

        itemCount: mappedData.length,
        separatorBuilder: (context, index) => Divider(),
        itemBuilder: (context, index) {
          return ListTile(
            leading: Text(
              mappedData.keys.elementAt(index),
              style:
                  TextStyle(fontSize: SizeConfig().getProportionateHeight(17),fontWeight: FontWeight.w400),
            ),
            trailing: Icon(mappedData.values.elementAt(index),color: kPrimaryColor,),
          );
        });
  }
}
