import 'package:flutter/material.dart';

import '../../modules.dart';

class PackageBody extends StatelessWidget {
  final ServiceCenter serviceCenter;
  const PackageBody({required this.serviceCenter, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PackageHeader(serviceCenter: serviceCenter),
        PackageBottom(serviceCenter: serviceCenter),
       
      ],
    );
  }
}
