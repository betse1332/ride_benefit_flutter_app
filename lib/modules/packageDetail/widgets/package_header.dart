import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/modules/packageDetail/widgets/working_hour_widget.dart';
import 'package:ride_benefit/utils/utils.dart';

import '../../modules.dart';

class PackageHeader extends StatelessWidget {
  final ServiceCenter serviceCenter;

  const PackageHeader({required this.serviceCenter, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset(serviceCenter.imagePath,
            width: SizeConfig.screenWidth,
            fit: BoxFit.cover,
            height: SizeConfig.screenHeight * .3),
        PackageDetail(serviceCenter: serviceCenter),
        PackageFragment(
          serviceCenter: serviceCenter,
          child: SpanWidget(
            spanTime: serviceCenter.servicePackages.workingHours,
          ),
        )
      ],
    );
  }
}
