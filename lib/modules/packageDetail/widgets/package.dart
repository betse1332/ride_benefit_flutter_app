import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

import '../../service/services.dart';

class Package extends StatelessWidget {
  final ServicePackage servicePackage;

  const Package({required this.servicePackage, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          final state =
              BlocProvider.of<RouterBloc>(context).state as ServiceRouteState;
          BlocProvider.of<RouterBloc>(context).add(NavigateToPackageDetail(
              pageConfig: packageDetailPageConfig,
              serviceNavConfig:
                  state.serviceNavConfig.copyWith(selectedPackage: 0)));
        },
        child: Card(
          elevation: 5,
          child: Column(children: [
            Padding(
              padding: EdgeInsets.only(
                  top: SizeConfig().getProportionateHeight(10),
                  left: SizeConfig().getProportionateWidth(20)),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(servicePackage.packageName,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig().getProportionateHeight(18),
                        )),
                    DiscountWidget(
                      discount: servicePackage.discount,
                    )
                  ]),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig().getProportionateWidth(20),
                  vertical: SizeConfig().getProportionateHeight(10)),
              child: Text(
                servicePackage.description,
                style: TextStyle(height: 1.7, wordSpacing: 2),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  right: SizeConfig().getProportionateWidth(20),
                  left: SizeConfig().getProportionateWidth(20),
                  bottom: SizeConfig().getProportionateHeight(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TagText(tagContent: drivers_tag),
                  TagText(tagContent: drivers_family_tag),
                  TagText(tagContent: corporate_tag)
                ],
              ),
            )
          ]),
        ));
  }
}
