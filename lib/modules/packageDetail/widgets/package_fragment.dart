import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/utils/utils.dart';

class PackageFragment extends StatelessWidget {
  final ServiceCenter serviceCenter;
  final Widget child;
  const PackageFragment({required this.child, required this.serviceCenter, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().getProportionateWidth(20),
          vertical: SizeConfig().getProportionateHeight(10)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TagText(tagContent: drivers_tag),
              TagText(tagContent: drivers_family_tag),
              TagText(tagContent: corporate_tag)
            ],
          ),
          Divider(
            height: SizeConfig().getProportionateHeight(30),
            thickness: 1.3,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Address",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    serviceCenter.address,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.grey),
                  )
                ],
              ),
              child
            ],
          )
        ],
      ),
    );
  }
}
