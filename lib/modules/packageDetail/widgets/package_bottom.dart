import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/utils/palette.dart';

class PackageBottom extends StatelessWidget {
  final ServiceCenter serviceCenter;

  const PackageBottom({required this.serviceCenter, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: SizeConfig().getProportionateHeight(130),
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig().getProportionateWidth(20)),
          child: GoogleMap(
              initialCameraPosition: CameraPositionConfig.getCameraPosition(
                  LatLng(serviceCenter.latitude, serviceCenter.longitude))),
        ),
        Container(
            width: double.infinity,
            height: SizeConfig().getProportionateHeight(80),
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig().getProportionateHeight(15),
                horizontal: SizeConfig().getProportionateWidth(20)),
            child: ElevatedButton(
              style: AppStyleConfig.getElevatedButtonStyle(
                  kPrimaryColor, roundedRadius_10),
              onPressed: () {
                final routerBloc = BlocProvider.of<RouterBloc>(context);
                final state = routerBloc.state as ServiceRouteState;
                routerBloc.add(NavigateToPackageBooking(
                    pageConfig: packageBookingPageConfig,
                    serviceNavConfig: state.serviceNavConfig));
              },
              child: Text(
                book_button,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig().getProportionateHeight(15)),
              ),
            ))
      ],
    );
  }
}
