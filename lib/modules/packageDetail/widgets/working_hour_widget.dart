import 'package:flutter/material.dart';
import 'package:ride_benefit/utils/utils.dart';

class SpanWidget extends StatelessWidget {
  final dynamic spanTime;
  const SpanWidget({required this.spanTime,Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          spanTime is String ? "Working Hrs" :"Tomorrow 8:00 pm",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Text(
          spanTime is String ? spanTime : "$spanTime left",
          style: TextStyle(
              fontWeight: FontWeight.bold, color: kPrimaryColor),
        )
      ],
    );
  }
}
