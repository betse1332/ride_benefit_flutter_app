import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/utils/utils.dart';

class DiscountWidget extends StatelessWidget {
  final int discount;
  const DiscountWidget({required this.discount, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(

        padding: EdgeInsets.symmetric(
            horizontal: SizeConfig().getProportionateWidth(15),
            vertical: SizeConfig().getProportionateHeight(13)),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.shade100,
                  spreadRadius: -3.0,
                  blurRadius: 3.0,
                  offset: Offset(3, 0))
            ],
            color: kPrimaryColor,
            borderRadius: BorderRadius.horizontal(left: Radius.circular(25))),
        child: Text("$discount% off",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: SizeConfig().getProportionateHeight(18),
            )));
  }
}
