import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';

class PackageDetail extends StatelessWidget {
  final ServiceCenter serviceCenter;

  const PackageDetail({required this.serviceCenter, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
              top: SizeConfig().getProportionateHeight(8),
              left: SizeConfig().getProportionateWidth(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ServiceCenterLabel(
                serviceCenterName: serviceCenter.centerName,
                description: serviceCenter.category,
              ),
              DiscountWidget(
                discount: serviceCenter.servicePackages.discount,
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig().getProportionateWidth(20),
              right: SizeConfig().getProportionateWidth(20),
              top: SizeConfig().getProportionateHeight(20)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                serviceCenter.servicePackages.packageName,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig().getProportionateHeight(17)),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: SizeConfig().getProportionateHeight(7)),
                child: Text(serviceCenter.servicePackages.description,
                    style: TextStyle(height: 1.5, wordSpacing: 5)),
              ),
            ],
          ),
        )
      ],
    );
  }
}
