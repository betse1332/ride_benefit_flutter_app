import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/modules/modules.dart';


class PackageScreen extends StatelessWidget {
  final ServiceCenter serviceCenter;
  const PackageScreen({required this.serviceCenter});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(child: PackageBody(serviceCenter: serviceCenter)),
    );
  }
}
