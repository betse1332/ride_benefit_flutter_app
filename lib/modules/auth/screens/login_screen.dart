import 'package:flutter/material.dart';

import '../auth.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Expanded(
            child: Align(alignment: Alignment.center, child: AuthHeader())),
        Expanded(
            child: Align(alignment: Alignment.topCenter, child: AuthBody()))
      ],
    ));
  }
}
