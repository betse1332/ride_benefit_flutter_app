import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

class AuthBody extends StatelessWidget {
  const AuthBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().getProportionateWidth(40.0)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 0),
            child: Text("Enter your phone number to login to the system"),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig().getProportionateHeight(20)),
            child: TextField(
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(
                      vertical: SizeConfig().getProportionateHeight(17)),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                      ),
                      borderRadius: BorderRadius.circular(roundedRadius_50)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: kPrimaryColor,
                          width: SizeConfig().getProportionateWidth(1.7)),
                      borderRadius: BorderRadius.circular(50)),
                  prefixIcon: Padding(
                    padding: EdgeInsets.only(
                        left: SizeConfig().getProportionateWidth(20)),
                    child: Text(
                      "+251  ",
                      style: TextStyle(
                          color: kPrimaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig().getProportionateHeight(16)),
                    ),
                  ),
                  // prefix: Container(
                  //   height: 20,
                  //   width: 20,
                  //   alignment: Alignment.center,
                  //   child: VerticalDivider(
                  //     thickness: 1,
                  //     color: Colors.black,
                  //   ),
                  // ),
                  prefixIconConstraints:
                      BoxConstraints(minHeight: 0, minWidth: 0),
                  hintText: "  Phone Number"),
            ),
          ),
          SizedBox(
            width: SizeConfig.screenWidth,
            height: SizeConfig().getProportionateHeight(50),
            child: ElevatedButton(
              style: AppStyleConfig.getElevatedButtonStyle(kPrimaryColor,
                  roundedRadius_50),
              onPressed: () {
                print("Simple routing");
                BlocProvider.of<RouterBloc>(context)
                    .add(NavigateTo(page: dashBoardPageConfig));
              },
              child: Text(
                "Continue",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: TextButton(
              onPressed: () {},
              child: Text(
                "Forget Password",
                style: TextStyle(
                    color: kPrimaryColor, fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}
