import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

class AuthHeader extends StatelessWidget {
  const AuthHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          ride_icon,
          height: SizeConfig.screenHeight * .1,
        ),
        Text(
          appTitle,
          style: TextStyle(
              fontSize: SizeConfig().getProportionateHeight(20),
              color: kPrimaryColor,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}
