export 'splash/splash.dart';
export 'auth/auth.dart';
export 'dashboard/dashboard.dart';
export 'service/services.dart';
export 'packageBooking/package_booking.dart';
export 'packageDetail/package_detail.dart';
export 'serviceCenter/service_center.dart';
export 'profile/profile.dart';
export 'bookings/bookings.dart';
export 'account/account.dart';
export 'notifications/notifications.dart';
export 'history/history.dart';

