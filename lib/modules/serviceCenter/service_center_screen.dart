import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';



class ServiceCenterScreen extends StatelessWidget {
  final ServiceCenter serviceCenter;
  const ServiceCenterScreen({required this.serviceCenter});



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: ServiceCenterBody(serviceCenter: serviceCenter),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
