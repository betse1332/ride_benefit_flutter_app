import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';

import '../../modules.dart';



class ServiceCenterBody extends StatelessWidget {
  final ServiceCenter serviceCenter;
  const ServiceCenterBody({required this.serviceCenter, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(children: [
        ServiceCenterHeader(serviceCenter: serviceCenter),
        Padding(
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig().getProportionateWidth(10),
                vertical: SizeConfig().getProportionateHeight(20)),
            child: Package(servicePackage: serviceCenter.servicePackages))
      ]),
    );
  }
}
