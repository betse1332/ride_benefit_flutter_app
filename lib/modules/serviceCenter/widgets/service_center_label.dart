import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';

class ServiceCenterLabel extends StatelessWidget {
  final String serviceCenterName;
  final String description;
  const ServiceCenterLabel(
      {required this.serviceCenterName, required this.description, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.only(top: SizeConfig().getProportionateHeight(10)),
          child: Text(
            serviceCenterName,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().getProportionateHeight(17)),
          ),
        ),
        Text(description,
            style: TextStyle(
              fontSize:  SizeConfig().getProportionateHeight(15),
                color: Colors.grey[500], fontWeight: FontWeight.bold)),
      ],
    );
  }
}
