import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/utils/utils.dart';

import '../../modules.dart';

class ServiceCenterHeader extends StatelessWidget {
  const ServiceCenterHeader({required this.serviceCenter, Key? key})
      : super(key: key);
  final ServiceCenter serviceCenter;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 5,
      shadowColor: Colors.grey[100],
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(serviceCenter.imagePath,
              width: SizeConfig.screenWidth,
              fit: BoxFit.cover,
              height: SizeConfig.screenHeight * .3),
          Padding(
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig().getProportionateHeight(15),
                  horizontal: SizeConfig().getProportionateWidth(18)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ServiceCenterLabel(
                      serviceCenterName: serviceCenter.centerName,
                      description: serviceCenter.category),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: SizeConfig().getProportionateHeight(13),
                    ),
                    child: Text(serviceCenter.description,
                        style: TextStyle(wordSpacing: 2, height: 1.7)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: SizeConfig().getProportionateHeight(20)),
                    child: Row(children: [
                      Text("Address",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize:
                                  SizeConfig().getProportionateHeight(17))),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig().getProportionateWidth(20)),
                        child: Text(serviceCenter.address,
                            style: TextStyle(
                                fontSize:
                                    SizeConfig().getProportionateHeight(17),
                                fontWeight: FontWeight.bold,
                                color: Colors.black54)),
                      ),
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.screenWidth * .25),
                    child: Text("Discount Offers",
                        style: TextStyle(
                            color: kPrimaryColor,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig().getProportionateHeight(18))),
                  )
                ],
              )),
        ],
      ),
    );
  }
}
