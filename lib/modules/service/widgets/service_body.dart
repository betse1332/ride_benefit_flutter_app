import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';

import '../../modules.dart';

class ServiceBody extends StatelessWidget {
  const ServiceBody({Key? key, required this.serviceCategory})
      : super(key: key);
  final ServiceCategory serviceCategory;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: SizeConfig().getProportionateHeight(25),
          horizontal: SizeConfig().getProportionateWidth(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            serviceCategory.serviceName,
            style: TextStyle(
                fontSize: SizeConfig().getProportionateHeight(17),
                fontWeight: FontWeight.w700),
          ),
          Expanded(
            child: GridView.builder(
              itemCount: serviceCategory.serviceCenters.length,
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200,
                childAspectRatio:
                    SizeConfig.screenWidth / (SizeConfig.screenHeight / 2.1),
                crossAxisSpacing: SizeConfig().getProportionateWidth(10),
                mainAxisSpacing: SizeConfig().getProportionateHeight(20),
              ),
              itemBuilder: (BuildContext context, index) {
                return ServiceItem(
                    onTap: () {
                      final state =
                          BlocProvider.of<RouterBloc>(context, listen: false)
                              .state as ServiceRouteState;
                      BlocProvider.of<RouterBloc>(context)
                          .add(NavigateToServiceCenter(
                        pageConfig: serviceCenterPageConfig,
                        serviceNavConfig: state.serviceNavConfig
                            .copyWith(selectedServiceCenter: index),
                      ));
                    },
                    serviceCenter: serviceCategory.serviceCenters[index]);
              },
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig().getProportionateHeight(15)),
            ),
          ),
        ],
      ),
    );
  }
}
