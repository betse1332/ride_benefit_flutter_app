import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';

import '../../modules.dart';



class ServiceItem extends StatelessWidget {
  const ServiceItem(
      {Key? key, required this.serviceCenter, required this.onTap})
      : super(key: key);
  final ServiceCenter serviceCenter;
  final VoidCallback onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: SizeConfig.screenHeight * .15,
            child: Image.asset(
              serviceCenter.imagePath,
              fit: BoxFit.fitHeight,
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(top: SizeConfig().getProportionateHeight(10)),
            child: Text(
              serviceCenter.centerName,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().getProportionateHeight(15)),
            ),
          ),
          Text(serviceCenter.category,
              style: TextStyle(
                  color: Colors.grey[500], fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }
}
