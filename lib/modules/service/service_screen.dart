import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';

class ServiceScreen extends StatelessWidget {
  const ServiceScreen({required this.serviceCategory});

  final ServiceCategory serviceCategory;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: ServiceBody(serviceCategory: serviceCategory),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
