import 'models.dart';

class ServiceCenter {
  final String centerName;
  final String imagePath;
  final String description;
  final String address;
  final double latitude;
  final double longitude;

  final String category;

  final ServicePackage servicePackages;
  ServiceCenter(
      {required this.centerName,
      required this.category,
      required this.address,
      required this.longitude,
      required this.latitude,
      required this.servicePackages,
      required this.imagePath,
      required this.description});
}
