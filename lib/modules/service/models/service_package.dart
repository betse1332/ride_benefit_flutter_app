class ServicePackage {
  final String packageName;
  final int discount;
  final String description;
  final String workingHours;

  ServicePackage(
      {required this.packageName,
      required this.workingHours,
      required this.description,
      required this.discount});
}
