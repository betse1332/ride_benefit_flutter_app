import 'models.dart';

class ServiceCategory {
  final String serviceName;
  final List<ServiceCenter> serviceCenters;

  ServiceCategory({required this.serviceName, required this.serviceCenters});
}
