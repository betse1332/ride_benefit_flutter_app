import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/modules/modules.dart';

class ProfileScreen extends StatelessWidget {
  final ProfileModel profileModel;


  const ProfileScreen({required this.profileModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ProfileBody(
        profileModel: profileModel,
      ),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
