import 'package:flutter/material.dart';

class ProfileBottomItem extends StatelessWidget {
  final String itemName;
  final Icon itemIcon;

  const ProfileBottomItem(
      {required this.itemName, required this.itemIcon, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          itemName,
          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 30),
        ),
        itemIcon
      ],
    );
  }
}
