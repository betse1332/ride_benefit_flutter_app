import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/palette.dart';

class ProfileBottom extends StatelessWidget {
  const ProfileBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(),
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            BlocProvider.of<RouterBloc>(context)
                .add(NavigateTo(page: profileItems[index].pageConfig));
          },
          child: ListTile(
            leading: Text(
              profileItems[index].itemName,
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: SizeConfig().getProportionateHeight(17)),
            ),
            trailing: Icon(
              profileItems[index].iconData,
              color: kPrimaryColor,
            ),
          ),
        );
      },
      itemCount: profileItems.length,
    );
  }
}
