import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';

class ProfileBody extends StatelessWidget {
  final ProfileModel profileModel;

  const ProfileBody({required this.profileModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ProfileHeader(
          profileModel: profileModel,
        ),
        Expanded(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig().getProportionateWidth(20)),
          child: ProfileBottom(),
        ))
      ],
    );
  }
}
