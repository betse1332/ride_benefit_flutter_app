import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/utils/utils.dart';

class ProfileHeader extends StatelessWidget {
  final ProfileModel profileModel;
  final double widgetSize;

  const ProfileHeader(
      {this.widgetSize = .32, required this.profileModel, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.screenHeight * widgetSize,
      width: SizeConfig.screenWidth,
      color: Palette.kSecondaryColor.shade100,
      padding: EdgeInsets.only(top: SizeConfig().getProportionateHeight(20)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig().getProportionateHeight(15)),
            child: CircleAvatar(
              backgroundColor: kPrimaryColor,
              radius: 40,
              child: Icon(
                CustomIcons.user,
                color: Palette.kSecondaryColor.shade900,
                size: SizeConfig().getProportionateHeight(30),
              ),
            ),
          ),
          Text("${profileModel.firstName}  ${profileModel.lastName}"),
          Text("+251 ${profileModel.phoneNum}"),
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig().getProportionateHeight(15)),
            child: Card(
              child: Padding(
                  child: Text(
                    profileModel.userTag,
                    style: TextStyle(
                        color: kPrimaryColor, fontWeight: FontWeight.bold),
                  ),
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig().getProportionateWidth(40),
                      vertical: SizeConfig().getProportionateHeight(10))),
            ),
          )
        ],
      ),
    );
  }
}
