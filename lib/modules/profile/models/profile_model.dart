class ProfileModel {
  final String firstName;
  final String lastName;
  final String phoneNum;
  final String userTag;
  final String email;

  ProfileModel(
      {required this.email,
      required this.firstName,
      required this.lastName,
      required this.phoneNum,
      required this.userTag});
}
