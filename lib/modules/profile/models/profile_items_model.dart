import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';

class ProfileItemModel {
  final String itemName;
  final IconData iconData;
  final PageConfiguration pageConfig;

  ProfileItemModel(
      {required this.iconData,
      required this.itemName,
      required this.pageConfig});
}
