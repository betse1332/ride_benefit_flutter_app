import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/modules/packageBooking/widgets/calendar.dart';
import 'package:ride_benefit/utils/utils.dart';

class PackageBookingBody extends StatelessWidget {
  final ServiceCenter serviceCenter;

  const PackageBookingBody({required this.serviceCenter, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: SizeConfig().getProportionateHeight(20),
          horizontal: SizeConfig().getProportionateWidth(20)),
      child: Container(
        height: SizeConfig.screenHeight * .85,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig().getProportionateHeight(90)),
              child: Column(
                children: [
                  Text(
                    booking_header,
                    style: TextStyle(
                        fontSize: SizeConfig().getProportionateHeight(17),
                        fontWeight: FontWeight.bold),
                  ),
                  Calendar()
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: SizeConfig().getProportionateHeight(50),
              child: ElevatedButton(
                  style: AppStyleConfig.getElevatedButtonStyle(
                      kPrimaryColor, roundedRadius_10,),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return ConfirmDialogBox(serviceCenter: serviceCenter);
                        });
                  },
                  child: Text(continue_button,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig().getProportionateHeight(15)))),
            ),
          ],
        ),
      ),
    );
  }
}
