import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/palette.dart';

class ConfirmDialogBoxBottomWidget extends StatelessWidget {
  final String centerAddress;
  final String date;

  const ConfirmDialogBoxBottomWidget(
      {required this.centerAddress, required this.date, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().getProportionateWidth(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    address,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    centerAddress,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.grey[400]),
                  )
                ],
              ),
              Text(
                date,
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig().getProportionateHeight(13)),
            child: ElevatedButton(
              onPressed: () {

                BlocProvider.of<RouterBloc>(context)
                    .add(NavigateTo(page: bookingStatusPageConfig));
                Navigator.pop(context);
              },
              child: Text(
                confirmBtn,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig().getProportionateHeight(15)),
              ),
              style: AppStyleConfig.getElevatedButtonStyle(
                  kPrimaryColor, roundedRadius_10, 18),
            ),
          ),
          ElevatedButton(
              onPressed: () => Navigator.pop(context),
              child: Text(
                cancelBtn,
                style: TextStyle(
                    fontSize: SizeConfig().getProportionateHeight(15),
                    fontWeight: FontWeight.bold),
              ),
              style: AppStyleConfig.getElevatedButtonStyle(
                  kSecondaryColor, roundedRadius_10, 18))
        ],
      ),
    );
  }
}
