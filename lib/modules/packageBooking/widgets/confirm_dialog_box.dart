import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/modules/modules.dart';

class ConfirmDialogBox extends StatelessWidget {
  const ConfirmDialogBox({required this.serviceCenter, Key? key})
      : super(key: key);
  final ServiceCenter serviceCenter;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(roundedRadius_10)),
      backgroundColor: Colors.white,
      child: Container(
          padding:
              EdgeInsets.only(top: SizeConfig().getProportionateHeight(20)),
          height: SizeConfig.screenHeight * .485,
          child: Column(children: [
            PackageDetail(serviceCenter: serviceCenter),

            ConfirmDialogBoxBottomWidget(
                centerAddress: serviceCenter.address, date: "Jun 23")
          ])),
    );
  }
}
