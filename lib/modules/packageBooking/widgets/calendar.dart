import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/utils/utils.dart';
import 'package:table_calendar/table_calendar.dart';

class Calendar extends StatefulWidget {
  const Calendar({Key? key}) : super(key: key);

  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  DateTime _focusedDay = DateTime.now();
  DateTime _selectedDay = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: SizeConfig().getProportionateHeight(30)),
      child: Material(
        elevation: 1.5,
        child: TableCalendar(

          onDaySelected: (selectedDay, focusedDay) {
            setState(() {
              _selectedDay = selectedDay;
              _focusedDay = focusedDay;
            });
          },
          selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
          headerStyle: HeaderStyle(
            titleTextStyle: TextStyle(fontWeight: FontWeight.bold),
            leftChevronMargin: EdgeInsets.only(left: 60),
            rightChevronMargin: EdgeInsets.only(right: 60),
            leftChevronIcon: Container(
                child: Icon(Icons.chevron_left), padding: iconPadding),
            rightChevronIcon: Container(
              child: Icon(Icons.chevron_right),
              padding: iconPadding,
            ),
            titleCentered: true,
            formatButtonVisible: false,
          ),
          daysOfWeekStyle: DaysOfWeekStyle(
              weekendStyle: TextStyle(fontWeight: FontWeight.bold),
              weekdayStyle: TextStyle(fontWeight: FontWeight.bold)),
          calendarStyle: CalendarStyle(
              defaultTextStyle: TextStyle(fontWeight: FontWeight.bold),
              selectedDecoration:
                  BoxDecoration(shape: BoxShape.circle, color: kPrimaryColor),
              todayDecoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.grey)),
          rowHeight: SizeConfig().getProportionateHeight(40),
          firstDay: DateTime.utc(2010, 10, 16),
          lastDay: DateTime.utc(2030, 3, 14),
          focusedDay: _focusedDay,
        ),
      ),
    );
  }
}
