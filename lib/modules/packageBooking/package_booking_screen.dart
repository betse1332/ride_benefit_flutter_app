import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/modules/modules.dart';

class PackageBookingScreen extends StatelessWidget {
  final ServiceCenter serviceCenter;

  const PackageBookingScreen({required this.serviceCenter, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(),
        body: SingleChildScrollView(
            child: PackageBookingBody(
          serviceCenter: serviceCenter,
        )));
  }
}
