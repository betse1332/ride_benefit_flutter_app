import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';

import 'package:ride_benefit/utils/utils.dart';

class SplashBottom extends StatelessWidget {
  const SplashBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding:
              EdgeInsets.only(right: SizeConfig().getProportionateHeight(68)),
          child: Text(
            "Powered by",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                height: SizeConfig().getProportionateHeight(0)),
          ),
        ),
        Text('RIDE Inc',
            style: TextStyle(
                color: kPrimaryColor,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().getProportionateHeight(38),
                height: SizeConfig().getProportionateHeight(1.1))),
        Padding(
          padding:
              EdgeInsets.only(left: SizeConfig().getProportionateWidth(54)),
          child: Text(
            'Beta Version',
            style: TextStyle(fontWeight: FontWeight.bold, height: SizeConfig().getProportionateHeight(.4)),
          ),
        )
      ],
    );
  }
}
