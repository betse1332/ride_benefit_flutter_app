import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/utils/datetime_util.dart';
import 'package:ride_benefit/utils/palette.dart';

class NotificationCard extends StatelessWidget {
  final NotificationModel notificationModel;

  const NotificationCard({required this.notificationModel, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().getProportionateWidth(10),
          vertical: SizeConfig().getProportionateHeight(8)),
      child: Card(
        elevation: 5,
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig().getProportionateWidth(10),
              vertical: SizeConfig().getProportionateHeight(13)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    bottom: SizeConfig().getProportionateHeight(5)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      notificationModel.serviceCenterName,
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: SizeConfig().getProportionateHeight(16)),
                    ),
                    notificationModel.isNew
                        ? CircleAvatar(
                            backgroundColor: kPrimaryColor,
                            radius: 10,
                            child: Text(
                              "1",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        : Text("")
                  ],
                ),
              ),
              RichText(
                  text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text:
                      "You have booked to our offer package, you can come and get our services after 3 days which is   ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                      height: SizeConfig().getProportionateHeight(1.5)),
                ),
                TextSpan(
                    text: DateTimeUtil.formatDatetime(notificationModel.date),
                    style: TextStyle(
                        color: kPrimaryColor, fontWeight: FontWeight.bold))
              ])),
            ],
          ),
        ),
      ),
    );
  }
}
