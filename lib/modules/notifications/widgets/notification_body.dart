import 'package:flutter/material.dart';
import 'package:ride_benefit/modules/modules.dart';

class NotificationBody extends StatelessWidget {
  final List<NotificationModel> notifications;

  const NotificationBody({required this.notifications, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

   notifications.sort((a,b){
     if(b.isNew) {
       return 1;
     }
     return -1;
   });

    return ListView.builder(
        itemCount: notifications.length,
        itemBuilder: (context, index) {
          return NotificationCard(notificationModel: notifications[index]);
        });
  }
}
