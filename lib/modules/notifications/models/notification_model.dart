class NotificationModel {
  final String serviceCenterName;
  final bool isNew;

  final DateTime date;

  NotificationModel(
      {required this.serviceCenterName,
      required this.isNew,

      required this.date});
}
