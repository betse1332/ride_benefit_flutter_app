import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';

class NotificationScreen extends StatelessWidget {
  final List<NotificationModel> notifications;

  const NotificationScreen({required this.notifications, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          titleSpacing: 0,
          leading: IconButton(
              icon: Icon(
                Icons.chevron_left,
                size: SizeConfig().getProportionateHeight(35),
              ),
              onPressed: () {
                Navigator.pop(context, true);
              }),
          title: Text("Notifications")),
      body: NotificationBody(
        notifications: notifications,
      ),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
