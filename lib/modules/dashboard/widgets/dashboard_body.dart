import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

import '../dashboard.dart';

class DashboardBody extends StatelessWidget {
  const DashboardBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: SizeConfig().getProportionateHeight(25),
          horizontal: SizeConfig().getProportionateWidth(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            chooseServices,
            style: TextStyle(
                fontSize: SizeConfig().getProportionateHeight(17),
                fontWeight: FontWeight.w700),
          ),
          Expanded(
            child: GridView.builder(
              itemCount: dashboardItems.length,
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: SizeConfig.screenWidth * .5,
                childAspectRatio:
                    SizeConfig.screenWidth / (SizeConfig.screenHeight / 2.3),
                crossAxisSpacing: SizeConfig().getProportionateWidth(3),
                mainAxisSpacing: SizeConfig().getProportionateHeight(10),
              ),
              itemBuilder: (BuildContext context, index) {
                return DashboardItem(
                    onTap: () {
                      print("ON tap VoidCallback $index");
                      BlocProvider.of<RouterBloc>(context).add(
                          NavigateToService(
                              serviceNavConfig: ServiceNavConfiguration(
                                  selectedService: 0),
                              pageConfig: servicePageConfig));
                    },
                    dashboardItemContent: dashboardItems[index]);
              },
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig().getProportionateHeight(15)),
            ),
          ),
        ],
      ),
    );
  }
}
