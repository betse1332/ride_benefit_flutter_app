import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

class DashBoardHeader extends StatelessWidget {
  const DashBoardHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 10,
      shadowColor: Colors.grey[100],
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: SizeConfig().getProportionateWidth(12),
            vertical: SizeConfig().getProportionateHeight(6)),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(),
              child: SvgPicture.asset(
                ride_icon,
                height: SizeConfig.screenHeight * .027,
              ),
            ),
            Expanded(child: SearchBar()),
            Stack(
              children: [
                Icon(Icons.notifications_outlined,
                    size: SizeConfig().getProportionateHeight(30)),
                Positioned(
                  right: 0,
                  top: 0,
                  child: new Container(
                    padding:
                        EdgeInsets.all(SizeConfig().getProportionateHeight(1)),
                    decoration: new BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(
                          SizeConfig().getProportionateHeight(10)),
                    ),
                    constraints: BoxConstraints(
                        minWidth: SizeConfig().getProportionateWidth(16),
                        minHeight: SizeConfig().getProportionateHeight(16)),
                    child: new Text(
                      '2',
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: SizeConfig().getProportionateHeight(12),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
