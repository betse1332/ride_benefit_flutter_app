import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';

import '../dashboard.dart';

class DashboardItem extends StatelessWidget {
  final DashboardItemModel dashboardItemContent;
  final VoidCallback onTap;
  const DashboardItem({Key? key, required this.dashboardItemContent,required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Card(
        elevation: 4,
        shadowColor: Colors.grey[100],
        child: Padding(
          padding:
              EdgeInsets.only(right: SizeConfig().getProportionateWidth(5)),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                dashboardItemContent.icon,
                Padding(
                  padding: EdgeInsets.only(
                      left: SizeConfig().getProportionateWidth(5),
                      top: SizeConfig().getProportionateHeight(20)),
                  child: Text(dashboardItemContent.lable,
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: SizeConfig().getProportionateHeight(15))),
                )
              ]),
        ),
      ),
    );
  }
}
