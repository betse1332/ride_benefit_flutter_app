import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

import '../dashboard.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        title: SearchBar(),
        leading: Padding(
          padding:
              EdgeInsets.only(left: SizeConfig().getProportionateWidth(10)),
          child: SvgPicture.asset(
            ride_icon,
            height: SizeConfig.screenHeight * .01,
          ),
        ),
        actions: [NotificationCounter()],
      ),
      body: DashboardBody(),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
