import 'package:flutter/material.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

class DashboardItemModel {
  final String lable;
  final Icon icon;

  DashboardItemModel({required this.lable, required this.icon});
}

List<DashboardItemModel> dashboardItems = [
  DashboardItemModel(
      lable: health_care,
      icon: Icon(
        CustomIcons.health_care,
        size: dashboardIconSize,
      )),
  DashboardItemModel(
      lable: sport_fitness,
      icon: Icon(
        CustomIcons.sport_fitness,
        size: dashboardIconSize,
      )),
  DashboardItemModel(
      lable: insurance,
      icon: Icon(
        CustomIcons.insurance,
        size: dashboardIconSize,
      )),
  DashboardItemModel(
      lable: rental,
      icon: Icon(
        CustomIcons.rental,
        size: dashboardIconSize,
      )),
  DashboardItemModel(
      lable: education,
      icon: Icon(
        CustomIcons.education,
        size: dashboardIconSize,
      )),
  DashboardItemModel(
      lable: market,
      icon: Icon(
        CustomIcons.shopping,
        size: dashboardIconSize,
      ))
];
