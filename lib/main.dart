import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/router/router.dart';

import 'package:ride_benefit/utils/utils.dart';

import 'bloc_observer.dart';

void main() {
  final PageManager pageManager = PageManager();
  final AppRouter routerDelegate = AppRouter(pageManager: pageManager);

  Bloc.observer = SimpleBlocObserver();
  runApp(App(
    routerDelegate: routerDelegate,
  ));
}

class App extends StatelessWidget {
  final AppRouter routerDelegate;

  App({required this.routerDelegate});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RouterBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            scaffoldBackgroundColor: Palette.kSecondaryColor.shade50,
            bottomNavigationBarTheme: BottomNavigationBarThemeData(
                backgroundColor: Palette.kSecondaryColor.shade50),
            appBarTheme:
                AppBarTheme(backgroundColor: Palette.kSecondaryColor.shade50),
            primaryColor: kPrimaryColor,
            primarySwatch: Palette.kPrimaryColor),
        home: Router(
          backButtonDispatcher:
              AppBackButtonDispatcher(routerDelegate: routerDelegate),
          routerDelegate: routerDelegate,
        ),
      ),
    );
  }
}
