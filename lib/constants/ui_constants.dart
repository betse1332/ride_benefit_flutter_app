import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/modules/modules.dart';
import 'package:ride_benefit/utils/utils.dart';

const appTitle = "Ride Benefit";
const double roundedRadius_50 = 50.0;
const double roundedRadius_10 = 10.0;
const String health_care = "Health Care";
const String sport_fitness = "Sport & Fitness";
const String insurance = "Insurance";
const String rental = "Rental";
const String education = "Education";
const String chooseServices = "Choose Services";
const String dashboardTab = "Dashboard";
const String bookingTab = "Bookings";
const String profileTab = "Profile";
const String market = "Market";
const String book_button = "Book Now";
const String booking_header = "Choose available dates for booking";
const String continue_button = "Continue";
const String drivers_tag = "Drivers";
const String drivers_family_tag = "Drivers Family";
const String corporate_tag = "Corporate";
const String address = "Address";
const String confirmBtn = "Confirm";
const String cancelBtn = "Cancel";
const String bookings_title = "Bookings";
const String congratulations_text = "Congratulations";
const String success_msg = "You have successfully booked the offered package";
const String check_msg = "Check to the place after 3 days from now";
const String go_to_home_btn = "Go to Home";

const Map<String, IconData> profileBottomItems = {
  "Account": CustomIcons.user,
  "Bookings": CustomIcons.booking_icon,
  "History": Icons.history,
  "Notifications": Icons.notifications_outlined,
  "Settings": Icons.settings_outlined,
  "About": Icons.info_outlined
};

List<ProfileItemModel> profileItems = [
  ProfileItemModel(iconData: CustomIcons.user,
      itemName: "Account",
      pageConfig: accountPageConfig),
  ProfileItemModel(iconData: Icons.history,
      itemName: "History",
      pageConfig: historyPageConfig),
  ProfileItemModel(iconData: Icons.settings_outlined,
      itemName: "Settings",
      pageConfig: settingsPageConfig),
  ProfileItemModel(iconData: Icons.info_outlined,
      itemName: "About",
      pageConfig: aboutPageConfig)

];
