import 'package:ride_benefit/constants/ui_constants.dart';
import 'package:ride_benefit/modules/modules.dart';

import 'constants.dart';

List<ServiceCenter> serviceCenters = [
  ServiceCenter(
    centerName: "Tikur Anbessa",
    imagePath: tikur_anbessa,
    category: "Specialization Hospital",
    description:
        "The this leave if if more be and bird.So gently door name less the tell sir the, aptly wheeled thy comete",
    address: "Zambia St,Addis Ababa",
    latitude: 9.021309427635828,
    longitude: 38.749507755566015,
    servicePackages: servicePackages[0],
  ),
  ServiceCenter(
      centerName: "Bete Zata",
      imagePath: bete_zata,
      latitude: 9.012989631792442,
      longitude: 38.75500465742046,
      servicePackages: servicePackages[0],
      address: "Addis Ababa Next to Stadium ",
      description:
          'El el se arcos alegrísima llanura los queman, sero gustada ni de lenta de nino tránsito con. Vilo conre ',
      category: "General Hospital"),
  ServiceCenter(
      centerName: "St George",
      address: "Zambia St,Addis Ababa",
      servicePackages: servicePackages[0],
      latitude: 9.00651773050179,
      longitude: 38.69993882282406,
      description:
          'Tale essilio come primo manifestamente che,dierio viviamo una audaci eterni e, allo esperienza che che ',
      imagePath: saint_george,
      category: "Higher Clinic"),
  ServiceCenter(
      centerName: "Addis Ababa Silk Road",
      imagePath: abeba,
       latitude: 8.984236621055393,
      servicePackages: servicePackages[0],
       longitude: 38.73797730345622,
      address: "Zambia St,Addis Ababa",
      description:
          'Que los las que donde de sus acaba tránsito, pisan la y tierra los se, el de es viciosa ciudades lunarter ',
      category: "General Hospital"),
  ServiceCenter(
      centerName: "St.Paulos Hospital",
      imagePath: ayele_beyene,
      latitude: 9.047519363448544,
      longitude:  38.728055055566195,
     
      servicePackages: servicePackages[0],
     
      address: ' Zambia St,Addis Ababa',
      description:
          'Perched the its the napping of soul or. And that floor off is heaven lore it blessed, flown some followesom',
      category: "Higher Clinic"),
  ServiceCenter(
      centerName: "Senay Hospital",
      address: "Zambia St,Addis Ababa",
      servicePackages: servicePackages[0],
      latitude: 8.981513018985817,
      longitude: 38.757793431851624,
      description:
          'Erat justo sed kasd clita aliquyam.  Takimata steter kasd no nonumy eos kasd eirmod amet invidunt, ei.',
      imagePath: lancha,
      category: "Dental Clinic")
];

List<ServiceCategory> serviceCategories = [
  ServiceCategory(serviceName: health_care, serviceCenters: serviceCenters)
];
