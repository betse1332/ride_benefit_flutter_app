import 'package:ride_benefit/modules/modules.dart';

import 'constants.dart';

ProfileModel userProfile = ProfileModel(
    email: "abebekebede@gmail.com",
    firstName: "Abebe",
    lastName: "Kebede",
    phoneNum: "9 23 45 67 89",
    userTag: "Driver");

List<NotificationModel> notificationsMock = [
  NotificationModel(
      serviceCenterName: serviceCenters[0].centerName,
      isNew: false,
      date: DateTime.utc(2021, 5, 23)),
  NotificationModel(
      serviceCenterName: serviceCenters[1].centerName,
      isNew: true,
      date: DateTime.utc(2021, 6, 21)),
  NotificationModel(
      serviceCenterName: serviceCenters[2].centerName,
      isNew: false,
      date: DateTime.utc(2021, 4, 12)),
  NotificationModel(
      serviceCenterName: serviceCenters[3].centerName,
      isNew: true,
      date: DateTime.utc(2021, 6, 28)),
  NotificationModel(
      serviceCenterName: serviceCenters[0].centerName,
      isNew: false,
      date: DateTime.utc(2021, 5, 23)),
  NotificationModel(
      serviceCenterName: serviceCenters[1].centerName,
      isNew: true,
      date: DateTime.utc(2021, 6, 21)),
  NotificationModel(
      serviceCenterName: serviceCenters[2].centerName,
      isNew: false,
      date: DateTime.utc(2021, 4, 12)),
  NotificationModel(
      serviceCenterName: serviceCenters[3].centerName,
      isNew: true,
      date: DateTime.utc(2021, 6, 28))
];

List<HistoryModel> histories = [
  HistoryModel(
      serviceCenterName: serviceCenters[0].centerName,
      category: serviceCenters[0].category,
      discount: servicePackages[0].discount,
      address: serviceCenters[0].address,
      date: DateTime.utc(2021, 5, 23)),
  HistoryModel(
      serviceCenterName: serviceCenters[1].centerName,
      category: serviceCenters[1].category,
      discount: servicePackages[0].discount,
      address: serviceCenters[1].address,
      date: DateTime.utc(2021, 6, 21)),
  HistoryModel(
      serviceCenterName: serviceCenters[2].centerName,
      category: serviceCenters[2].category,
      discount: servicePackages[0].discount,
      address: serviceCenters[2].address,
      date: DateTime.utc(2021, 4, 12)),
  HistoryModel(
      serviceCenterName: serviceCenters[0].centerName,
      category: serviceCenters[0].category,
      discount: servicePackages[0].discount,
      address: serviceCenters[0].address,
      date: DateTime.utc(2021, 5, 23)),
  HistoryModel(
      serviceCenterName: serviceCenters[1].centerName,
      category: serviceCenters[1].category,
      discount: servicePackages[0].discount,
      address: serviceCenters[1].address,
      date: DateTime.utc(2021, 6, 21)),
  HistoryModel(
      serviceCenterName: serviceCenters[2].centerName,
      category: serviceCenters[2].category,
      discount: servicePackages[0].discount,
      address: serviceCenters[2].address,
      date: DateTime.utc(2021, 4, 12)),
];
