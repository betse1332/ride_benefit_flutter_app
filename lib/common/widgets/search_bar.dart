import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(

      margin: EdgeInsets.symmetric(
          vertical: SizeConfig().getProportionateHeight(5),
          horizontal: SizeConfig().getProportionateWidth(10)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.screenHeight * .053,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: TextField(

          keyboardType: TextInputType.text,
          cursorColor: Colors.grey,
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(
              decoration: TextDecoration.none, fontSize: 20, color: Colors.black),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 12),
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,

            hintText: "Search",
            hintStyle: TextStyle(color: Colors.grey[600]),
            filled: true,
            fillColor: Colors.grey[200],
            prefixIcon: Icon(Icons.search,
                color: Colors.grey[600], size: SizeConfig.screenHeight * .03),
          ),
        ),
      ),
    );
  }
}
