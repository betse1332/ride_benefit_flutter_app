import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/constants/constants.dart';
import 'package:ride_benefit/utils/utils.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RouterBloc, RouterState>(builder: (context, state) {
      return Container(
        decoration: BoxDecoration(boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.shade300,
              blurRadius: 10,
              offset: const Offset(0, .75))
        ]),
        child: BottomNavigationBar(
          elevation: 10,
          onTap: (int index) {
            AppRouter.bottomNavigationHelper(index, context);
          },
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          currentIndex: (state.props[0] as PageConfiguration).selectedTab,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.grid_view_sharp),
              label: dashboardTab,
            ),
            BottomNavigationBarItem(
                icon: Icon(CustomIcons.booking_icon), label: bookingTab),
            BottomNavigationBarItem(
                icon: Icon(CustomIcons.profile_icon), label: profileTab)
          ],
        ),
      );
    });
  }
}
