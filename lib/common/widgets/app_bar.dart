import 'package:flutter/material.dart';
import 'package:ride_benefit/common/common.dart';
import 'package:ride_benefit/config/config.dart';



class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      titleSpacing: 0,
      leading: IconButton(
          icon: Icon(
            Icons.chevron_left,
            size: SizeConfig().getProportionateHeight(35),
          ),
          onPressed: () {
            Navigator.pop(context, true);
          }),
      title: SearchBar(),
      actions: [NotificationCounter()],
    );
  }

  @override
  Size get preferredSize =>
      Size(SizeConfig.screenWidth, SizeConfig.screenHeight * .068);
}
