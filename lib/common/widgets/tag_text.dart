import 'package:flutter/material.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/utils/palette.dart';

class TagText extends StatelessWidget {
  const TagText({required this.tagContent, Key? key}) : super(key: key);
  final String tagContent;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().getProportionateWidth(10),
          vertical: SizeConfig().getProportionateHeight(5)),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: Palette.kPrimaryColor, width: 2)),
      child: Text(
        tagContent,
        style: TextStyle(fontWeight: FontWeight.w500),
      ),
    );
  }
}
