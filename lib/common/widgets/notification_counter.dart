import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ride_benefit/config/config.dart';
import 'package:ride_benefit/utils/utils.dart';

class NotificationCounter extends StatelessWidget {
  const NotificationCounter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final state = BlocProvider.of<RouterBloc>(context, listen: false);

        ServiceNavConfiguration serviceNav = state.state.props.length > 1
            ? state.state.props[1] as ServiceNavConfiguration
            : ServiceNavConfiguration();

        BlocProvider.of<RouterBloc>(context).add(NavigateToNotification(
            pageConfig: notificationPageConfig,
            notificationPopConfig: NotificationPopConfig(
                serviceConfig: serviceNav,
                pageConfig: state.state.props[0] as PageConfiguration)));
      },
      child: Padding(
        padding: EdgeInsets.only(
            top: SizeConfig().getProportionateHeight(14),
            right: SizeConfig().getProportionateWidth(10)),
        child: Stack(
          children: [
            Icon(Icons.notifications_outlined,
                size: SizeConfig().getProportionateHeight(30)),
            Positioned(
              right: 0,
              top: 0,
              child: new Container(
                padding: EdgeInsets.all(SizeConfig().getProportionateHeight(1)),
                decoration: new BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.circular(
                      SizeConfig().getProportionateHeight(10)),
                ),
                constraints: BoxConstraints(
                    minWidth: SizeConfig().getProportionateWidth(16),
                    minHeight: SizeConfig().getProportionateHeight(16)),
                child: new Text(
                  '4',
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: SizeConfig().getProportionateHeight(12),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
