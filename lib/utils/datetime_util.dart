import 'package:intl/intl.dart';
class DateTimeUtil {
  static String formatDatetime(DateTime dateTime) {
    return "${DateFormat('MMMM').format(dateTime)} ${dateTime.day} - ${dateTime.year}";
  }
}
