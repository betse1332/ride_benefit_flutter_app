import 'package:ride_benefit/config/config.dart';

double dashboardIconSize = SizeConfig.screenHeight * .08;
const double kToolbarHeight = 56.0;
double itemHeight = (SizeConfig.screenHeight - kToolbarHeight - 24) / 2;
double itemWidth = SizeConfig.screenWidth / 2;
