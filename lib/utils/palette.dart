import 'package:flutter/material.dart';

Color kPrimaryColor = Palette.kPrimaryColor.shade500;
Color kSecondaryColor = Palette.kSecondaryColor.shade200;

class Palette {
  static MaterialColor kPrimaryColor = MaterialColor(0xFFFFD500, <int, Color>{

    50: const Color(0xFFFFFBE5), //10%
    100: const Color(0xFFFFF7CC), //20%
    200: const Color(0xFFFFEE99), //30%
    300: const Color(0xFFFFE666), //40%
    400: const Color(0xFFFFDD33), //50%
    500: const Color(0xFFFFD500), //60%
    600: const Color(0xFFCCAA00), //70%
    700: const Color(0xFF998000), //80%
    800: const Color(0xFF665500), //90%
    900: const Color(0xFF332B00), //100%
  });

  static MaterialColor kSecondaryColor = MaterialColor(0xFF9E9E9E, <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFFFAFAFA),
    200: const Color(0xFFEBEBEB),
    300: const Color(0xFFD1D1D1),
    400: const Color(0xFFB8B8B8),
    500: const Color(0xFF9E9E9E),
    600: const Color(0xFF858585),
    700: const Color(0xFF6B6B6B),
    800: const Color(0xFF525252),
    900: const Color(0xFF383838)
  });
}
